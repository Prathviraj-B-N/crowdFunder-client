import React, { useState, useEffect } from "react";
import { useStateContext } from "../context";
import { DisplayCampaigns } from "../components";
import { daysLeft } from "../utils";

const Home = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [campaigns, setCampaigns] = useState([]);

  const { address, contract, getCampaigns } = useStateContext();

  const filterCampaigns = (campaign) => {
    return daysLeft(campaign.deadline) > 0; 
  }

  const fetchCampaigns = async () => {
    const data = await getCampaigns();
    const filteredData = data.filter(filterCampaigns);
    setCampaigns(filteredData);
    setIsLoading(false);
  };

  useEffect(() => {
    setIsLoading(true)
    if (contract) fetchCampaigns();
  }, [address, contract]);

  return (
    <div>
      
      <DisplayCampaigns
        title="All Campaigns"
        isloading={isLoading}
        campaigns={campaigns}
      />
    </div>
  );
};

export default Home;
